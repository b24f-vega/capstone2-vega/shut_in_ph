const mongoose = require("mongoose")

let bookingSchema = mongoose.Schema({
    userId:{
        type:String,
        required:[
            true, "User ID is required!"
        ]
    },
    userEmail:{
        type:String,
        required:[
            true, "User Name is required!"
        ]
    },
    serviceId:{
        type:String,
        required:[
            true, "Service ID is required!"
        ]
    },
    serviceName:{
        type:String,
        required:[
            true, "Service is required!"
        ]
    },
    scheduledOn:{
        type:Date,
        default:new Date()
    }
    ,status:{
        type:String,
        default:"Pending"
    }
});


module.exports = mongoose.model("booking",bookingSchema);