const mongoose = require("mongoose")

let serviceSchema = new mongoose.Schema({
    name:{
        type:String,
        required:[
            true, "Name is required!"
        ]
    },
    price:{
        type:Number,
        required:[
            true, "Price is required!"
        ]
    },
    description:{
        type:String,
        required:[
            true, "Description is required!"
        ]
    },
    image:{
        type:String,
        required:[
            true, "Image is required!"
        ]
    },
    isActive:{
        type:Boolean,
        default:true
    },
    createdOn:{
        type:Date,
        default:new Date()
    }

});




module.exports = mongoose.model("service",serviceSchema);