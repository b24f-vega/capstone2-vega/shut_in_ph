const mongoose = require("mongoose")

let userSchema = new mongoose.Schema({
    fullname :{
        type:String,
        required:[
            true, "Full Name is required!"
        ]
    },
    email:{
        type:String,
        required:[
            true, "Email is required!"
        ]
    },
    password:{
        type:String,
        required:[
            true, "Password is required!"
        ]
    },
    address:{
        type:String,
        required:[
            true, "Address is required!"
        ]
    },
    mobileNo:{
        type:Number,
        required:[
            true, "Mobile Number is required!"
        ]
    },
    isAdmin:{
        type:Boolean,
        default: false
    }
});




module.exports = mongoose.model("user",userSchema);