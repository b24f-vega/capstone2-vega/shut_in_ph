
import './App.css';
import AppNavBar from './components/appNavBar.js'
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login'
import {BrowserRouter as Router,Routers,Route, Routes} from 'react-router-dom'
import Logout from './pages/Logout';
import NotFound from './components/NotFound';
import { UserProvider } from './UserContext';
import { useEffect, useState } from 'react';
import CourseView from './components/CourseView';
function App() {
  const [user,setUser] = useState(localStorage.getItem({id:null,isAdmin:null}))
  const unSetUser = () =>{
    localStorage.clear();
  }
  useEffect(()=> {
    console.log(user);
  },[user])
  return (
    <UserProvider value={{user,setUser,unSetUser}}>
        <Router >
          <AppNavBar/>
          <Routes>
            <Route path ='/' element={<Home/>}/>
            <Route path ='/Courses' element={<Courses/>}/>
            <Route path ='/Login' element={<Login/>}/>
            <Route path ='/Register' element={<Register/>}/>
            <Route path ='/Logout' element={<Logout/>}/>
            <Route path='*' element={<NotFound />}/>
            <Route path='/course/:courseId' element={<CourseView/>}/>
          </Routes>
        </Router>
    </UserProvider>
    
  );
}

export default App;
