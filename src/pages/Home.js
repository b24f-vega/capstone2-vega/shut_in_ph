import Banner from "../components/Banner.js";
import Highlights from "../components/Highlights.js";
import { Fragment } from "react";
// import CourseCard from "../components/CourseCard.js";
import { Container } from "react-bootstrap";

export default function Home(){
    return(
        <Fragment>
        <Container>
            <Banner/>
        </Container>
        <Container>
            <Highlights/>
        </Container>
        </Fragment>
    )
}