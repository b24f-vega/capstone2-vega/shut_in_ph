import CourseCard from '../components/CourseCard'
import { Fragment, useEffect, useState } from 'react'
export default function Courses(){

    const[courses,setCourses] = useState([]);

    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/course/allActive`)
        .then(result => result.json())
        .then(data =>{
            console.log(data)
            setCourses(data.map(course =>{
				return(<CourseCard key = {course._id} courseProp = {course}/>)
			}))
        })
    },[])

    return(
        <Fragment>
            <h1 className='mt-3 text-center'>Courses</h1>
            {courses}
        </Fragment>
        )
}