import { Row, Col,Button} from 'react-bootstrap'
import Card from 'react-bootstrap/Card';
import { useEffect, useState,useContext } from 'react';
import UserContext from '../UserContext';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}){
    
    const {_id,name,description,price} = courseProp;
    const [enrollees,setEnrollees] = useState(0);
    const [seats, setSeats] = useState(30);
    const [isDisabled,setIsDisabled] = useState(false);
    const {user}= useContext(UserContext);

    useEffect(()=> {
        if(seats === 0){
            setIsDisabled(true);
        }
    },[seats])
    
    // function enroll(){
    //     if(seats===0 && enrollees ===30){
    //         alert(`no`);
    //     }
    //     else{
    //         setEnrollees(enrollees+1);
    //         setSeats(seats-1);
    //     }
    // }
    




    return(
        
        <Row>
        <Col className = 'mt-5 col-10 mx-auto'>
            <Card >
                <Card.Body >
                    <Card.Text><strong>{name}</strong></Card.Text>
                    <Card.Text>{description}</Card.Text>
                    <Card.Text><strong>P{price}</strong></Card.Text>
                    <Card.Text>Enrollees:</Card.Text>
                    <Card.Text>{enrollees}</Card.Text>
                    <Card.Text>Available Seats:</Card.Text>
                    <Card.Text>{seats}</Card.Text>
                    {
                        user?
                        <Button as = {Link} to = {`/course/${_id}`} disabled = {isDisabled}>see more details</Button>
                        :
                        <Button as={Link} to='/Login' >Login</Button>
                    }
                    
                </Card.Body>
            </Card>           
        </Col>
    </Row>
    
    )
}