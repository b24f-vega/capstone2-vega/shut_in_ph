
/* 
    //ShutInPH
    //Housekeeping Services for Introverts
    //We Solemnly Swear You'll never go out again

*/
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./Routes/userRoutes.js");
const serviceRoutes =require("./Routes/serviceRoutes.js")

const port = 3001;
const app = express();

//[Middlewares Section]
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

//[Connection String to MongoDB Server]
mongoose.set('strictQuery',true);
mongoose.connect("mongodb+srv://admin:admin@batch245-vega.ien0lsm.mongodb.net/Booking_API_Vega?retryWrites=true&w=majority",{
    useNewUrlParser:true,
    useUnifiedTopology:true
});
let db = mongoose.connection;
//[Error Handling]
db.on("error",console.error.bind(console,"Connection Error!"))

//[Validation]
db.once("open",()=>{console.log("We are connected to the cloud!")})

//[Routes Section]
app.use("/user",userRoutes);
app.use("/service",serviceRoutes);




app.listen(port,() => {
    console.log(`Server is running at port :${port}`)
})