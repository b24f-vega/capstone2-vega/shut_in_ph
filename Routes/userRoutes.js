const express = require("express");
const userControllers = require("../Controllers/userControllers");
const router = express.Router();
const auth = require("../Controllers/authControllers");


router.post("/register",userControllers.addUser);
router.post("/login",userControllers.userAuthentication);
router.get("/details",auth.verify,userControllers.getDetail);
router.get("/checkServices",auth.verify,userControllers.checkServices);
router.get("/checkPendingServices",auth.verify,userControllers.checkPendingServices);
router.get('/allUser',auth.verify, userControllers.getAllUsers)


router.put("/setAdmin/:userId",auth.verify,userControllers.setAdmin);
router.post("/availService/:serviceId",auth.verify,userControllers.availService);
router.put("/cancelService/:serviceId",auth.verify,userControllers.cancelService);
module.exports = router;