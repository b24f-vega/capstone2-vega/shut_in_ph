// [Imports and Declaration Section]
const express = require("express");
const serviceControllers = require("../Controllers/serviceControllers.js");
const auth = require("../Controllers/authControllers.js");
const router = express.Router();

//[Routes Section]

router.post("/addService",auth.verify, serviceControllers.addService);
router.get("/getActiveService",serviceControllers.getActiveService);
router.get("/:serviceId",serviceControllers.serviceDetails);
router.put("/updateService/:serviceId",auth.verify, serviceControllers.updateService);
router.put("/archiveService/:serviceId",auth.verify,serviceControllers.archiveService);

module.exports = router;