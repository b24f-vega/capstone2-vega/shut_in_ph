const mongoose = require("mongoose");
const service = require("../Models/serviceSchema.js")
const auth = require("./authControllers.js")

module.exports.addService = (request,response) =>{
    const adminData = auth.decode(request.headers.authorization);
    if(adminData.isAdmin == true){
        let input = request.body;
        let newService = new service({
            name : input.name,
            description: input.description,
            price: input.price,
            image: input.image
        })
        return newService.save().then(result => {
            return response.send(true);
        })
        .catch(error =>{
            return response.send(false);
        })
    }
    else{
        return response.send(false)
    }
}
module.exports.getActiveService = (request,response) =>{
    service.find({isActive:true}).then(result =>{
        return response.send(result)
    }).catch(error => response.send(false));
}
module.exports.serviceDetails = (request, response) => {
	const serviceId = request.params.serviceId;

	service.findById(serviceId)
	.then(result => response.send(result))
	.catch(error => response.send(false));
}
module.exports.updateService = async (request,response) =>{
    const adminData = auth.decode(request.headers.authorization);
    let serviceToUpdate = request.params.serviceId;
    let input = request.body
    if(adminData.isAdmin == true){
    await service.findById(serviceToUpdate).then(result => {
            if(result !== null){
                let change = {
                    name:input.name,
                    price:input.price,
                    description:input.description,
                    image:input.image
                }
                service.findByIdAndUpdate({_id:serviceToUpdate},change,{new:true})
                .then(result => {
                    return response.send(true);
                }).catch(error =>{
                    return response.send(false);
                });
            }
            else{
                return response.send(false);
            }
       })
    }
    else{
        return response.send(false);
    }
   
}
module.exports.archiveService = async (request,response) =>{
    const adminData = auth.decode(request.headers.authorization);
    let serviceToBeArchived = request.params.serviceId;
    if(adminData.isAdmin == true){
    await service.findByIdAndUpdate({_id:serviceToBeArchived},{isActive:false},{})
        .then(result => {
            response.send(true);
        }).catch(error =>{
            return response.send(error);
        });
    }
    else{
        return response.send(false);
    }
}
