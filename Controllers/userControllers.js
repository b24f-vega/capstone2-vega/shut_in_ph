const bcrypt = require("bcrypt");
const auth = require("./authControllers");
const user = require("../Models/userSchema.js");
const service = require("../Models/serviceSchema");
const booking = require("../Models/bookingSchema");
const moment = require('moment')
//[User Registration]

module.exports.addUser =(request,response) =>{
    let input = request.body
    user.findOne({email:input.email}).then(result =>{
        if(result !== null){
            return response.send(false);
        }
        else{
            let newUser = new user({
                fullname : input.fullname,
                password : bcrypt.hashSync(input.password,10),
                email : input.email,
                address : input.address,
                mobileNo : input.mobileNo
            });
            newUser.save().then(save => {
                return response.send(true);
            })
            .catch(error => response.send(false));
        }
    })
}
module.exports.getDetail = (request,response) => {
    const userData = auth.decode(request.headers.authorization);
    return user.findById(userData._id).then(result =>{
        result.password = "";
    return response.send(result);
    })
}
module.exports.userAuthentication = (request,response) =>{
    let input = request.body
    user.findOne({email:input.email}).then(result =>{
        if(result === null){
            return response.send("Email is not yet registered");
        }
        else{
            const isPasswordCorrect = bcrypt.compareSync(input.password,result.password);
            if(isPasswordCorrect){
                return response.send({auth:auth.createAccessToken(result)});
            }
            else{
                return response.send("Password is incorrect");
            }
        }
    }) 
}

module.exports.availService = (request,response) =>{
    const userData = auth.decode(request.headers.authorization);
    const input = request.body;
    const serviceId = request.params.serviceId;
    service.findById(serviceId).then(result =>{
        if(userData.isAdmin === false){
            
            let newBooking = new booking({
                userId : userData._id,
                userEmail:userData.email,
                serviceId : result._id,
                serviceName: result.name,
                scheduledOn : input.scheduledOn              
            });
            newBooking.save().then(result => {
                return response.send(true)
           })
            .catch(error =>{
                response.send(error)
            })
        }
        else{
            return response.send(false) 
        }
        
    })
    .catch(error => {
        response.send(error);
    })

}
module.exports.getAllUsers = (request,response) => {
    let adminData = auth.decode(request.headers.authorization)
    if(adminData.isAdmin){
       user.find({}).then(result =>{
            return response.send(result)
       })
       .catch(error =>{
            return response.send(false)
        })
    }
    else{
        response.send(false)
    }
}
module.exports.setAdmin =  (request,response) =>{
    let adminData = auth.decode(request.headers.authorization)
    let userId = request.params.userId
    if(adminData.isAdmin){
     user.findByIdAndUpdate({_id:userId},{isAdmin:true},{})
        .then(result =>{
            return response.send(true)
        })
        .catch(error =>{
            return response.send(error);
        });
    }
    else{
        return response.send(false)
    }
}
module.exports.checkServices = async (request,response) => {
    let userData = auth.decode(request.headers.authorization);
    await booking.find({userId : userData._id }).then(result => {
        return response.send(result);
    }) 
    .catch(error => {
        return response.send(error)
    })
}

module.exports.checkPendingServices = (request,response) => {
    let adminData = auth.decode(request.headers.authorization);
    if(adminData.isAdmin){
        booking.find({}).then(result => {
            return response.send(result);
        })
        .catch(error => {
            return response.send(error)
        })
    }
    else{
        return response.send("Not an Admin!")
    }
}
module.exports.cancelService = (request,response) => {
    let userData = auth.decode(request.headers.authorization);
    let serviceToBeCancelled = request.params.serviceId;
    let change ={
        status : "Cancelled"
    }
    booking.findByIdAndUpdate({_id:serviceToBeCancelled},change,{})
        .then(result => {
            response.send(true);
        }).catch(error =>{
            return response.send(error);
        });
}